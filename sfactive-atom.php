<?php
// TODO
// 2. do something with sf-active's 'display' variable
//    - at the very least, we need to know what content was hidden
//
// 4. get data for a site <author> section (the IMC itself)
//     - depends on #1, how we get configured

ini_set('memory_limit', '768M');

$options = getopt("f:t");
include($options['f']);
$test = isset($options['t']) ? true : false;

$base = 0;
$records = fetch_records($test);
file_put_contents(DB_USERNAME.".xml", emit_records($records));

// if test = true, then only peek at the database and emit some output to sanity check everything 
function fetch_records($test) {
	if(!mysql_connect(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD)) {
		print "unable to connect";
		exit(-1);
	}
	mysql_select_db(DB_DATABASE);

	$query = "SELECT id,created,article,parent_id,author,heading,arttype,linked_file,mime_type from webcast 
		  UNION
		  SELECT CONCAT('feature',feature_id),creation_date,summary AS article,NULL,NULL,NULL,NULL,NULL,NULL from feature
			WHERE status IN ('a','c') AND is_current_version=1";
	if($test) {
		$query .= " LIMIT 25";
	}
	print $query;
	$result = mysql_query($query);
	echo mysql_error();

	while($r = mysql_fetch_object($result)) {
	  foreach($r as $key => $value) {
		$r->$key = mb_convert_encoding($value, "UTF-8");

		// TODO ensure that we aren't breaking anything here
		$r->$key = nl2br($r->$key);

		// the theory of some random people on the internet here is that
		// these bytes should never ben in UTF-8.  I have a slightly hard
		// time believing that, but regardless, without this the 
		// perl XML parser used by xml_pp breaks, which isn't a good sign.
		// need to verify by running a UTF-8 document with all characters
		// TODO we might find on any IMC (regardless of language) through this
		// filter, or if we want to be lazier, spot checking that we aren't
		// breaking the accent marks of the sites we run through this.
		$r->$key = preg_replace("/[\\x00-\\x1F\\x80-\\xFF]/", "", $r->$key);
	  }
	  $objects[$r->id] = $r;
	}
	return $objects;
}
function emit_records($objects) {
	$doc = new DOMDocument("1.0", "UTF-8");
	$feeds = $doc->createElement("feeds");
	$feeds = $doc->createElementNS('http://www.w3.org/2005/Atom', 'feeds');
	$feeds->setAttributeNS('http://www.w3.org/2000/xmlns/' ,'xmlns:thr', "http://purl.org/syndication/thread/1.0");

	$author = $doc->createElement("author");
	$author->appendChild(element_textnode($doc, "name",$GLOBALS['site_name']));
	$feeds->appendChild($author);
	foreach($objects as $r) {
	  $entry = $doc->createElement("entry");

	  $entry->appendChild($doc->createElement("id", get_ref($r)));

	  // begin content block
	  $content = $doc->createElement("content");
	  $content->appendChild($doc->createTextNode(get_body($r)));
	  $content->setAttribute("type", "html");
	  $entry->appendChild($content);
	  // end content block

	  // begin author block
	  $author = $doc->createElement("author");
	  $author->appendChild(element_textnode($doc, "name",$r->author));
	  if(!empty($r->contact)) {
		  $author->appendChild(element_textnode($doc, "email",$r->contact));
	  }

	  // note - the following author elements aren't defined in the standard
	  // and probably won't be imported by any existing tools.
	  if(!empty($r->address)) {
		  $author->appendChild(element_textnode($doc, "address", $r->address));
	  }
	  if(!empty($r->phone)) {
		  $author->appendChild(element_textnode($doc, "phone", $r->phone));
	  }

	  $entry->appendChild($author);
	  // end author block

	  $entry->appendChild(element_textnode($doc, "published",date(DATE_ATOM,strtotime($r->created))));
	  $entry->appendChild(element_textnode($doc, "title", $r->heading));

	  if($r->parent_id) {
		  $inreply = $doc->createElement("thr:in-reply-to");
		  $inreply->setAttribute("ref",get_ref($objects[$r->parent_id]));
		  $inreply->setAttribute("href",get_href($objects[$r->parent_id]));
		  $entry->appendChild($inreply);
	  }

	  $feeds->appendChild($entry);
	}
	$doc->appendChild($feeds);
	return $doc->saveXML();
}


// create a parent node named $name that belongs to $doc,
// then create a text node that is the child of $name
// returns the parent node created
function element_textnode($doc, $name, $text) {
	$elem = $doc->createElement($name);
	$elem->appendChild($doc->createTextNode($text));
	return $elem;
}

function get_ref($r) {
	// TODO - does the exact format of this matter?  kindof cribbed this ID format
	// from blogger.  what does 1999 mean?  no fucking idea
	$site = $GLOBALS['site_crumb'];

	return "tag:sf-active,1999:site-$site.post-{$r->id}";
}
function get_href($r) {
	$time = strtotime($r->created);
	return SF_ROOT_URL."/news/".date("Y",$time)."/".date("m",$time)."/{$r->id}.php";
}

function get_body($r) {
	$body = "";

	// special case comments / attachments
	if($r->parent_id) {
		// further special case attachments
		if($r->arttype == "attachment") {
			// TODO - do we want to <embed> anything?  this is a good place to do it.
			// alternatively we can throw some wild classes on this and use javascript
			// to change the links into embeds... that sounds crazy but could be useful
			// for, say, turning all links to mp3s into embedded mp3 players
			if(preg_match("/^image/",$r->mime_type)) {
				return "<img class=\"sfa\" src=\"".get_attachment_url($r)."\">";
			} else {
				$body = "<a class='sfa-attach' href=\"".get_attachment_url($r)."\">{$r->heading}</a>";
				$body .= "<p>{$r->article}</p>";
				return $body;
			}
		}
		return $r->article;
	}
	if(!empty($r->summary)) {
		$body .= "<div class='sfa-summary'>{$r->summary}</div>";
	}
	if(!empty($r->article)) {
		$body .= "<div class='sfa-body'>{$r->article}</div>";
	}
	return $body;
}

// takes a row and gets the path to the linked_file -- i.e. strips
// off path through docroot
function get_attachment_url($r) {
	echo $r->linked_file;
	return preg_replace("#^.*".SF_WEB_PATH."#","",$r->linked_file);
}
